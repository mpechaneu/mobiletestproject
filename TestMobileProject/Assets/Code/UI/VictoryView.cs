﻿using UnityEngine;
using UnityEngine.UI;

namespace Code.UI
{
    public class VictoryView : MonoBehaviour
    {
        public Text FinalText;

        public void Initialize(string finalMessage)
        {
            FinalText.text = finalMessage;
        }
    }
}
