﻿using System;
using Code.Data;
using Code.Units;
using UnityEngine;
using UnityEngine.UI;

namespace Code.UI
{
    public class TooltipView : MonoBehaviour
    {
        public Text Hp;
        public Text Speed;
        public Text Attack;
        public Text Description;

        public void InitializeTooltip(GameUnit unit)
        {
            var unitStruct = GameDataProvider.Instance.GetUnit(unit.UnitTag);
            Description.text = unitStruct.Description;
            Hp.text = String.Format("{0}/{1}", unit.Hp, unitStruct.Health);
            Speed.text = unit.Speed.ToString();
            Attack.text = unit.Attack.ToString();
        }
    }
}
