﻿using System;

namespace Code.Services
{
    public class ShowHideStruct
    {
        public IShowHide Object;
        public bool Show;
        public float Time;
        public Action OnCompleted;
    }
}
