﻿using Code.Data;
using Code.Units;

namespace Code.Services
{
    public class DealDamageService
    {
        private readonly GameManager _manager;

        public DealDamageService(GameManager manager)
        {
            _manager = manager;
        }

        public void DealDamage(UnitWrapper attacker, UnitWrapper target)
        {
            var damageValue = attacker.Unit.Attack;
            var unitInfo = GameDataProvider.Instance.GetUnit(target.Unit.UnitTag);
            if (unitInfo.DamageResistance)
            {
                damageValue--;
            }
            target.Unit.Hp -= damageValue;
            if (target.Unit.Hp <= 0)
            {
                target.Unit.Dead = true;
            }
            target.UpdateHpbar();
        }
    }
}
