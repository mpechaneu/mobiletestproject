﻿namespace Code.Services
{
    public enum MoveState
    {
        None,
        FirstMove,
        SecondMove,
        EndMove
    }
}
