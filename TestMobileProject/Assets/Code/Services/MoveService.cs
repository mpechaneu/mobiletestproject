﻿using System;
using System.Collections.Generic;
using Code.Landscape;
using Code.Units;

namespace Code.Services
{
    public class MoveService
    {
        private const int TurnModifier = 1;

        private readonly GameManager _manager;
        private List<GameCell> _path;
        private int _currentIndex;
        private Action _onCompleted;

        public MoveService(GameManager manager)
        {
            _manager = manager;
        }

        public void MoveUnit(UnitWrapper unitWrapper, GameCell cell, Action onCompleted)
        {
            if (unitWrapper.GetCell() == cell)
            {
                OnCompleted();
                return;
            }
            var pathToCell = _manager.BfsService.FindPath(unitWrapper, cell, null, unitWrapper.Unit.Speed);
            _currentIndex = 0;
            _path = pathToCell;
            _onCompleted = onCompleted;
            ProcessNext();
        }

        private void ProcessNext()
        {
            if (_currentIndex == _path.Count - 1)
            {
                OnCompleted();
                return;
            }
            var unitToMove = GetUnitWrapper();
            if (ShouldTurnToCell(unitToMove.GetCell(), unitToMove.Unit.IsTurned(),  GetNextCell()))
            {
                _manager.TurnService.TurnUnit(unitToMove, MoveUnit);
            }
            else
            {
                MoveUnit();
            }
        }

        private UnitWrapper GetUnitWrapper()
        {
            return _path[_currentIndex].GetUnitOnCell();
        }

        private GameCell GetNextCell()
        {
            return _path[_currentIndex + 1];
        }

        private GameCell GetNextNextCell()
        {
            return _path[_currentIndex + 2];
        }

        private void MoveUnit()
        {
            _manager.CellMoveService.AnimateUnitMoving(GetUnitWrapper(), GetNextCell(), IsNextCellStops(), MoveCompleted);
        }

        private bool IsNextCellStops()
        {
            if (_currentIndex == _path.Count - 2)
                return true;
            if (ShouldTurnToCell(GetNextCell(), GetUnitWrapper().Unit.IsTurned(), GetNextNextCell()))
                return true;
            return false;
        }

        private void MoveCompleted()
        {
            _currentIndex++;
            ProcessNext();
        }

        private void OnCompleted()
        {
            var action = _onCompleted;
            _onCompleted = null;
            _path = null;
            if (action != null)
            {
                action();
            }
        }

        public static bool ShouldTurnToCell(GameCell cell1, bool unitTurned, GameCell cell2)
        {
            var cell1Bigline = cell1.IsOnBigline();
            var cell2Bigline = cell2.IsOnBigline();
            int modifier = 0;
            if (cell1Bigline != cell2Bigline)
            {
                if (cell1Bigline)
                {
                    modifier = 0;
                }
                else
                {
                    modifier = -TurnModifier;
                }
            }
            var dx = cell1.Dx - cell2.Dx - modifier;
            if (unitTurned && dx <= 0)
                return false;
            if (!unitTurned && dx > 0)
                return false;
            return true;
        }
    }
}
