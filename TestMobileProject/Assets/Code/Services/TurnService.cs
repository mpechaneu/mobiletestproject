﻿using System;
using Code.Units;
using AnimationState = Code.Units.AnimationState;

namespace Code.Services
{
    public class TurnService
    {
        private readonly GameManager _manager;

        private AnimationManager _unitAnimationManager;
        private UnitWrapper _unit;
        private Action _onCompleted;

        public TurnService(GameManager manager)
        {
            _manager = manager;
        }

        public void TurnUnit(UnitWrapper unit, Action onCompleted)
        {
            if (_unit != null)
                return;
            _unit = unit;
            _onCompleted = onCompleted;
            _unitAnimationManager = _unit.Unit.GetAnimationManager();
            _unitAnimationManager.TurnCompleted += TurnCompleted;
            _unitAnimationManager.AnimationState = AnimationState.Turning;
        }

        public void ReturnToTeamTurn(UnitWrapper unit, Action onCompleted)
        {
            if ((unit.Unit.Team == Constants.Team1 && unit.Unit.IsTurned()) ||
                (unit.Unit.Team == Constants.Team2 && !unit.Unit.IsTurned()))
            {
                TurnUnit(unit, onCompleted);
            }
            else
            {
                if (onCompleted != null)
                {
                    onCompleted();
                }
            }
        }

        private void TurnCompleted()
        {
            _unitAnimationManager.TurnCompleted -= TurnCompleted;
            _unit.Unit.SetTurn(!_unit.Unit.IsTurned());
            var action = _onCompleted;
            _onCompleted = null;
            _unit = null;
            _unitAnimationManager = null;
            if (action != null)
            {
                action();
            }
        }
    }
}
