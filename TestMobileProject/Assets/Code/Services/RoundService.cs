﻿using Code.Interaction;
using UnityEngine;

namespace Code.Services
{
    public class RoundService: MonoBehaviour
    {
        public GameManager Manager;

        public void StartRound(int team)
        {
            Manager.GameInfo.PlayerTurn = team;
            Manager.GameInfo.ReadyUnits();
            Manager.HightlightManager.HightlightDefaultTargets();
        }

        public void EndTurn()
        {
            Manager.GameInfo.PlayerTurn = GameInfo.GetOppositeTeam(Manager.GameInfo.PlayerTurn);
            Manager.GameInfo.ReadyUnits();
            Manager.HightlightManager.HightlightDefaultTargets();
        }
    }
}
