﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Landscape;
using Code.Units;

namespace Code.Services
{
    public class BfsService
    {
        public const int InitialCost = 1000;

        private readonly GameManager _manager;

        public BfsService(GameManager manager)
        {
            _manager = manager;
        }

        private List<GameCell> GetCells(UnitWrapper unit, int breadth, bool includeEnemies)
        {
            var resultSet = new HashSet<GameCell>();
            var cellDict = new Dictionary<GameCell, int>();
            var cells = _manager.Map.GetCells();
            foreach (var cell in cells)
            {
                cellDict.Add(cell, InitialCost);
            }
            var queue = new Queue<QueueStruct>();
            queue.Enqueue(new QueueStruct() { Cell = unit.GetCell(), Cost = 0});
            while (queue.Count>0)
            {
                var current = queue.Dequeue();
                var newCost = current.Cost+1;
                if (includeEnemies)
                {
                    var enemyCells = current.Cell.GetNearUnitsByTeam(GameInfo.GetOppositeTeam(unit.Unit.Team));
                    foreach (var enemyCell in enemyCells)
                    {
                        resultSet.Add(enemyCell);
                    }
                }
                if (newCost > breadth)
                    continue;
                var nearCells = current.Cell.GetEmptyNearCells();
                foreach (var gameCell in nearCells)
                {
                    if (cellDict[gameCell] > newCost)
                    {
                        resultSet.Add(gameCell);
                        queue.Enqueue(new QueueStruct() {Cell = gameCell, Cost = newCost});
                    }
                }
            }
            return resultSet.ToList();
        }

        public List<GameCell> FindPath(UnitWrapper unit, GameCell cellToFind, UnitWrapper targetEnemy, int breadth)
        {
            var cellDict = new Dictionary<GameCell, int>();
            var cells = _manager.Map.GetCells();
            foreach (var cell in cells)
            {
                cellDict.Add(cell, InitialCost);
            }
            var queue = new Queue<QueueStruct>();
            queue.Enqueue(new QueueStruct { Cell = unit.GetCell(), Cost = 0, Path = new List<GameCell> { unit.GetCell() } });
            while (queue.Count > 0)
            {
                var current = queue.Dequeue();
                if (cellToFind != null && current.Cell == cellToFind)
                {
                    return current.Path;
                }
                if (targetEnemy != null)
                {
                    var enemyCells = current.Cell.GetNearUnitsByTeam(GameInfo.GetOppositeTeam(unit.Unit.Team));
                    foreach (var enemyCell in enemyCells)
                    {
                        if (enemyCell.GetUnitOnCell() == targetEnemy)
                        {
                            return current.Path;
                        }
                    }
                }
                var newCost = current.Cost + 1;
                if (newCost > breadth)
                    continue;
                var nearCells = current.Cell.GetEmptyNearCells();
                foreach (var gameCell in nearCells)
                {
                    if (cellDict[gameCell] > newCost)
                    {
                        var newPath = new List<GameCell>(current.Path);
                        newPath.Add(gameCell);
                        queue.Enqueue(new QueueStruct { Cell = gameCell, Cost = newCost, Path = newPath });
                    }
                }
            }
            throw new Exception();
        }

        public List<GameCell> GetCellsToMove(UnitWrapper unit, bool ranged)
        {
            return GetCells(unit, unit.Unit.Speed, !ranged);
        }
    }
}
