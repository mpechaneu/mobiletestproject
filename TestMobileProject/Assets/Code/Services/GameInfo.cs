﻿using System.Collections.Generic;
using System.Linq;
using Code.Units;

namespace Code.Services
{
    public class GameInfo
    {
        public int PlayerTurn { get; set; }
        private readonly HashSet<UnitWrapper> _team1Units;
        private readonly HashSet<UnitWrapper> _team2Units;

        public GameInfo()
        {
            _team1Units = new HashSet<UnitWrapper>();
            _team2Units = new HashSet<UnitWrapper>();
        }

        public HashSet<UnitWrapper> GetUnits(int team)
        {
            if (team == Constants.Team1)
                return _team1Units;
            return _team2Units;
        }

        public void AddUnit(UnitWrapper unitWrapper)
        {
            GetUnits(unitWrapper.Unit.Team).Add(unitWrapper);
        }

        public List<UnitWrapper> GetReadyUnits()
        {
            return GetUnits(PlayerTurn).Where(unitWrapper => unitWrapper.Unit.IsReady()).ToList();
        }

        public void ReadyUnits()
        {
            foreach (var unitWrapper in GetUnits(PlayerTurn))
            {
                unitWrapper.Unit.SetReady(true);
            }
            foreach (var unitWrapper in GetUnits(GetOppositeTeam(PlayerTurn)))
            {
                unitWrapper.Unit.SetReady(false);
            }
        }

        public void RemoveUnit(UnitWrapper unitWrapper)
        {
            GetUnits(unitWrapper.Unit.Team).Remove(unitWrapper);
        }

        public static int GetOppositeTeam(int team)
        {
            if (team == Constants.Team1)
                return Constants.Team2;
            return Constants.Team1;
        }
    }
}
