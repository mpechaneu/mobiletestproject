﻿using System.Collections;
using Code.Interaction;
using Code.UI;
using Code.Units;
using UnityEngine;

namespace Code.Services
{
    public class TooltipSerivce: MonoBehaviour
    {
        private const float MinDelta = 2.5f;
        private const float WaitTime = 0.5f;

        public TooltipView TooltipView;
        public GameManager Manager;

        private TooltipView _tooltipView;
        private GameUnit _gameUnit;
        private IEnumerator _showToolip;

        public void StartViewingTooltip(GameUnit gameUnit)
        {
            if (_gameUnit == gameUnit)
                return;
            if (_tooltipView != null || _showToolip != null)
            {
                HideTooltip();
            }
            _gameUnit = gameUnit;
            _showToolip = ShowTooltop();
            StartCoroutine(_showToolip);
        }

        private IEnumerator ShowTooltop()
        {
            yield return new WaitForSeconds(WaitTime);
            ShowTooltipInstantly();
            _showToolip = null;
        }

        private void ShowTooltipInstantly()
        {
            var obj = Instantiate(TooltipView, transform, false);
            _tooltipView = obj.GetComponent<TooltipView>();
            _tooltipView.InitializeTooltip(_gameUnit);
        }

        public void OnMoveMove(Vector2 delta)
        {
            if (delta.magnitude > MinDelta)
            {
                HideTooltip();
            }
        }

        public void HideTooltip()
        {
            if (_showToolip != null)
            {
                StopCoroutine(_showToolip);
            }
            if (_tooltipView != null)
            {
                Destroy(_tooltipView.gameObject);

            }
            _showToolip = null;
            _tooltipView = null;
            _gameUnit = null;
        }
    }
}
