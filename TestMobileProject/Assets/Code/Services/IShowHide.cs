﻿namespace Code.Services
{
    public interface IShowHide
    {
        void SetAlpha(float alpha);
    }
}
