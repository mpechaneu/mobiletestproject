﻿using Boo.Lang;
using UnityEngine;

namespace Code.Services
{
    public class ArrowPositionHelper
    {
        private const float AlmostZero = 0.0001f;
        public const float Degree360 = 360f;
        public const float Degree90 = 90f;
        public const float Degree180 = 180f;
        public const float Degree270 = 270f;

        public static float GetCellEnterDegree(Vector2 movePosition, Vector2 cellCenter)
        {
            var vectDiff = movePosition - cellCenter;
            float angle = 0;
            if (Mathf.Abs(vectDiff.x) < AlmostZero)
            {
                if (vectDiff.y > 0)
                {
                    angle = Degree90;
                }
                else
                {
                    angle = Degree270;
                }
            }
            else
            {
                var polarCoords = CartesianToPolar(vectDiff);
                angle = polarCoords.y;
                if (angle < 0)
                {
                    angle = Degree360 + angle;
                }
                
            }
            //Debug.DrawLine(new Vector3(movePosition.x, movePosition.y, 1), new Vector3(cellCenter.x, cellCenter.y, -1), Color.red, 200, false);
            //Debug.Log(angle);
            return angle;
        }

        private static Vector2 CartesianToPolar(Vector2 point)
        {
            Vector2 polar;

            polar.x = point.magnitude;
            polar.y = Mathf.Atan2(point.y, point.x);

            //convert to deg
            polar.y *= Mathf.Rad2Deg;
 
            return polar;
        }
    }
}
