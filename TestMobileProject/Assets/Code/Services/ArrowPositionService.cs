﻿using System;
using System.Collections.Generic;
using Code.Landscape;
using UnityEngine;

namespace Code.Services
{
    public class ArrowPositionService: MonoBehaviour
    {
        public GameObject RightArrow;
        public GameObject TopRightArrow;
        public GameObject TopLeftArrow;
        public GameObject LeftArrow;
        public GameObject BottomLeftArrow;
        public GameObject BottomRightArrow;

        public GameManager Manager;
        private const float Atan05 = 26.56505118f;
        private const float MiddleValue = 0.5f;

        private GameObject _arrowObject;
        private int _direction;
        private GameCell _targetCell;

        public void ViewPositionArrow(Vector2 coords, GameCell cell)
        {
            var angle = ArrowPositionHelper.GetCellEnterDegree(coords, cell.transform.position);
            var availableDirections = GetAvailableDirections(cell);
            int nearestDirection = GetNearestDirection(availableDirections, angle);
            var prefab = GetPrefab(nearestDirection);
            _direction = nearestDirection;
            _targetCell = cell;
            _arrowObject = Instantiate(prefab, cell.transform, false);
        }

        public GameCell GetArrowCell()
        {
            return _targetCell.GetNearCell(_direction);
        }

        public bool IsArrowInitialized()
        {
            return _targetCell != null;
        }

        public void RemoveArrow()
        {
            if (_arrowObject != null)
            {
                Destroy(_arrowObject);
            }
            _direction = 0;
            _targetCell = null;
        }

        private List<int> GetAvailableDirections(GameCell cell)
        {
            var unitToActivate = Manager.InteractManager.GetUnitToActivate();
            var directions = cell.GetNearDirections();
            var nearCells = cell.GetNearCells();
            var availableDirections = new List<int>();
            int i;
            for (i=0; i< nearCells.Count; i++)
            {
                if (nearCells[i].GetInteractManager().GetHightlightCellState() == HightlightCellState.Hightlighted ||
                    unitToActivate.GetCell() == nearCells[i])
                {
                    availableDirections.Add(directions[i]);
                }
            }
            return availableDirections;
        }

        private int GetNearestDirection(List<int> availableDirections, float angle)
        {
            var directionValue = ConvertAngleToDirectionValue(angle);
            int bestDirectionIndex = 0;
            float bestDirectionDiff = Mathf.Abs(availableDirections[0] - directionValue);
            int i;
            for (i = 1; i < availableDirections.Count; i++)
            {
                var diff = Mathf.Abs(availableDirections[i] - directionValue);
                if (diff < bestDirectionDiff)
                {
                    bestDirectionIndex = i;
                    bestDirectionDiff = diff;
                }
            }
            return availableDirections[bestDirectionIndex];
        }

        private float ConvertAngleToDirectionValue(float angle)
        {
            if (angle < Atan05 || angle >= ArrowPositionHelper.Degree360 - Atan05)
                return ConvertToDirection(GameCell.Right, - Atan05, Atan05, ConvertAngle(angle)) ;
            if (angle >= Atan05 && angle < ArrowPositionHelper.Degree90)
                return ConvertToDirection(GameCell.TopRight, Atan05, ArrowPositionHelper.Degree90, angle);
            if (angle >= ArrowPositionHelper.Degree90 && angle < ArrowPositionHelper.Degree180 - Atan05)
                return ConvertToDirection(GameCell.TopLeft, ArrowPositionHelper.Degree90, ArrowPositionHelper.Degree180 - Atan05, angle);
            if (angle >= ArrowPositionHelper.Degree180 - Atan05 && angle < ArrowPositionHelper.Degree180 + Atan05)
                return ConvertToDirection(GameCell.Left, ArrowPositionHelper.Degree180 - Atan05, ArrowPositionHelper.Degree180 + Atan05, angle);
            if (angle >= ArrowPositionHelper.Degree180 + Atan05 && angle < ArrowPositionHelper.Degree270)
                return ConvertToDirection(GameCell.BottomLeft, ArrowPositionHelper.Degree180 + Atan05, ArrowPositionHelper.Degree270, angle);
            return ConvertToDirection(GameCell.BottomRight, ArrowPositionHelper.Degree270, ArrowPositionHelper.Degree360 - Atan05, angle);
        }

        private float ConvertToDirection(int value, float bottomValue, float upperValue, float angle)
        {
            return (float) value + (angle - bottomValue)/(upperValue - bottomValue) - MiddleValue;
        }

        private float ConvertAngle(float angle)
        {
            if (angle > ArrowPositionHelper.Degree180)
                return angle - ArrowPositionHelper.Degree360;
            return angle;
        }

        private GameObject GetPrefab(int nearestDirection)
        {
            switch (nearestDirection)
            {
                case GameCell.Right:
                    return RightArrow;
                case GameCell.TopRight:
                    return TopRightArrow;
                case GameCell.TopLeft:
                    return TopLeftArrow;
                case GameCell.Left:
                    return LeftArrow;
                case GameCell.BottomLeft:
                    return BottomLeftArrow;
                case GameCell.BottomRight:
                    return BottomRightArrow;
                default: throw new Exception();
            }
        }
    }
}
