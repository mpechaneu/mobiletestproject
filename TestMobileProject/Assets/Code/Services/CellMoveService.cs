﻿using System;
using System.Collections;
using Code.Data;
using Code.Landscape;
using Code.Units;
using UnityEngine;
using AnimationState = Code.Units.AnimationState;

namespace Code.Services
{
    public class CellMoveService: MonoBehaviour
    {
        public const float EndMoveBarrier = 0.8f;
        public GameManager Manager;

        private AnimationManager _unitAnimationManager;
        private UnitWrapper _unit;
        private Action _onCompleted;
        private GameCell _targetCell;
        private Vector2 _vect;
        private MoveState _moveState;
        private int _subMove;
        private bool _useSub;
        private bool _processEndMove;
        private GameManager _manager;

        public CellMoveService(GameManager manager)
        {
            _moveState = MoveState.None;
            _manager = manager;
        }

        public void AnimateUnitMoving(UnitWrapper unitToMove, GameCell targetCell, bool nextCellStops, Action onCompleted)
        {
            _onCompleted = onCompleted;
            _unit = unitToMove;
            _processEndMove = nextCellStops && IsComplexMove();
            _unitAnimationManager = _unit.Unit.GetAnimationManager();
            _targetCell = targetCell;
            _vect = new Vector2(targetCell.transform.position.x - unitToMove.GetCell().transform.position.x,
                                targetCell.transform.position.y - unitToMove.GetCell().transform.position.y);
            _unitAnimationManager.MoveCompleted += MoveCompleted;
            if (_unitAnimationManager.AnimationState == AnimationState.Moving)
            {
                _useSub = true;
            }
            else
            {
                _useSub = false;
            }
            _unitAnimationManager.AnimationState = AnimationState.Moving;
            var enumerator = WaitForNextFrame();
            StartCoroutine(enumerator);
        }

        private bool IsComplexMove()
        {
            return !GameDataProvider.Instance.GetUnit(_unit.Unit.UnitTag).SimpleMove;
        }

        private IEnumerator WaitForNextFrame()
        {
            yield return null;
            if (_useSub)
            {
                _subMove = (int)_unitAnimationManager.GetAnimationTime();
            }
            else
            {
                _subMove = 0;
            }
            
            _moveState = MoveState.FirstMove;
        }

        private void Update()
        {
            if (_moveState == MoveState.None)
                return;
            float time = 0;
            if (_moveState != MoveState.EndMove)
            {
                time = (_unitAnimationManager.GetAnimationTime() - _subMove) / 2;
                if (_processEndMove)
                    time *= EndMoveBarrier;
            }
            else
            {
                if (!_unitAnimationManager.IsEndMovePlaying())
                    return;
                time = EndMoveBarrier + _unitAnimationManager.GetAnimationTime() * (1 - EndMoveBarrier);
            }
            _unit.transform.localPosition = new Vector2(_vect.x * time, _vect.y * time);
        }

        private void MoveCompleted()
        {
            switch (_moveState)
            {
                case MoveState.FirstMove:
                    _moveState = MoveState.SecondMove;
                    _unit.GetCell().ClearUnit();
                    _targetCell.AddUnit(_unit);
                    break;
                case MoveState.SecondMove:
                    if (_processEndMove)
                    {
                        ProcessEndMove();
                    }
                    else
                    {
                        _unitAnimationManager.MoveCompleted -= MoveCompleted;
                        OnCompleted();
                    }
                    break;
                default: throw new Exception();
            }
        }

        private void ProcessEndMove()
        {
            _moveState = MoveState.EndMove;
            _unitAnimationManager.MoveCompleted -= MoveCompleted;
            _unitAnimationManager.MoveEndCompleted += EndMoveCompleted;
            _unitAnimationManager.AnimationState = AnimationState.EndMoving;
        }

        private void EndMoveCompleted()
        {
            _unitAnimationManager.MoveEndCompleted -= EndMoveCompleted;
            OnCompleted();
        }

        private void OnCompleted()
        {
            _unit.transform.SetParent(_targetCell.transform, false);
            _unit.transform.localPosition = new Vector2(0, 0);
            var action = _onCompleted;
            _onCompleted = null;
            _unit = null;
            _targetCell = null;
            _moveState = MoveState.None;
            _vect = Vector2.zero;
            _subMove = 0;
            _processEndMove = false;
            if (action != null)
            {
                action();
            }
        }
    }
}
