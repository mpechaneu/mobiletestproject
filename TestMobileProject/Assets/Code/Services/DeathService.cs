﻿using Code.Units;
using UnityEngine;

namespace Code.Services
{
    public class DeathService: MonoBehaviour
    {
        private GameManager _manager;

        public void Initialize(GameManager manager)
        {
            _manager = manager;
        }

        public void UnitDead(UnitWrapper unitWrapper)
        {
            unitWrapper.GetCell().ClearUnit();
            _manager.GameInfo.RemoveUnit(unitWrapper);
            Destroy(unitWrapper.gameObject);
            _manager.GameEndService.CheckEndOfTheGame();
        }
    }
}
