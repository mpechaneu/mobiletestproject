﻿using Code.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Code.Services
{
    public class GameEndService : MonoBehaviour
    {
        private const string Team2VictoryMessage = "Victory of the holy squad";
        private const string Team1VictoryMessage = "Victory of the evil squad";

        public VictoryView VictoryView;
        public GameManager Manager;
        public Button EndTurnButtom;

        public void CheckEndOfTheGame()
        {
            if (Manager.GameInfo.GetUnits(Constants.Team1).Count == 0)
            {
                OnGameEnd(Team2VictoryMessage);
            }
            if (Manager.GameInfo.GetUnits(Constants.Team2).Count == 0)
            {
                OnGameEnd(Team1VictoryMessage);
            }
        }

        private void OnGameEnd(string message)
        {
            var obj = Instantiate(VictoryView, transform, false);
            var victoryView = obj.GetComponent<VictoryView>();
            victoryView.Initialize(message);
            EndTurnButtom.enabled = false;
            Manager.InputManager.Disable();
            Manager.TooltipSerivce.HideTooltip();
        }
    }
}
