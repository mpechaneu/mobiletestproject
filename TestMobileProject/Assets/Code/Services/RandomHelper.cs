﻿using System;
using System.Collections.Generic;

namespace Code.Services
{
    public static class RandomHelper
    {
        private const int BoolValue = 2;
        private static readonly Random Random = new Random();

        public static T RandomElement<T>(this IList<T> list)
        {
            return list[Random.Next(list.Count)];
        }

        public static T RandomElement<T>(this T[] array)
        {
            return array[Random.Next(array.Length)];
        }

        public static int GetRandomInt(int max)
        {
            return Random.Next(max);
        }

        public static bool GetRandomBool()
        {
            return Random.Next(BoolValue) == 0;
        }

        public static int GetRandomPercent()
        {
            return Random.Next(0, 101);
        }
    }
}
