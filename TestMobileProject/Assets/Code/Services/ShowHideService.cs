﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Code.Services
{
    public class ShowHideService: MonoBehaviour
    {
        public const float TimeToChange = 0.5f;

#pragma warning disable 414
        private GameManager _manager;
#pragma warning restore 414
        private Dictionary<IShowHide, ShowHideStruct> _objects;
        private List<IShowHide> _toDelete;
        private List<Action> _actions;

        public void Initialize(GameManager manager)
        {
            _manager = manager;
            _toDelete = new List<IShowHide>();
            _objects = new Dictionary<IShowHide, ShowHideStruct>();
            _actions = new List<Action>();
        }

        public void Hide(IShowHide fadeOut, Action onCompleted)
        {
            AddFade(fadeOut, false, onCompleted);
        }

        public void Show(IShowHide fadeOut, Action onCompleted)
        {
            AddFade(fadeOut, true, onCompleted);
        }

        public void StopFade(IShowHide fadeOut, bool ignoreOnCompleted = false)
        {
            if (_toDelete.Contains(fadeOut))
                return;
            ShowHideStruct fadeStruct;
            if (_objects.TryGetValue(fadeOut, out fadeStruct))
            {
                _objects.Remove(fadeOut);
                if (!ignoreOnCompleted && fadeStruct.OnCompleted != null)
                    fadeStruct.OnCompleted();
            }
        }

        private void AddFade(IShowHide fadeOut, bool show, Action onCompleted)
        {
            ShowHideStruct fadeStruct;
            if (_objects.TryGetValue(fadeOut, out fadeStruct))
            {
                if (!_toDelete.Contains(fadeOut))
                {
                    _objects.Remove(fadeOut);
                    if (fadeStruct.OnCompleted != null)
                        fadeStruct.OnCompleted();
                }
            }
            fadeStruct = GetNewStruct(fadeOut, show, onCompleted);
            _objects.Add(fadeOut, fadeStruct);
        }

        private ShowHideStruct GetNewStruct(IShowHide fadeOut, bool show, Action onCompleted)
        {
            ShowHideStruct fadeOutStuct = new ShowHideStruct();
            fadeOutStuct.Object = fadeOut;
            fadeOutStuct.Show = show;
            fadeOutStuct.OnCompleted = onCompleted;
            if (show)
                fadeOut.SetAlpha(0);
            else fadeOut.SetAlpha(1);
            fadeOutStuct.Time = 0;
            return fadeOutStuct;
        }

        void Update()
        {
            if (_objects == null || _objects.Count == 0)
                return;
            foreach (var keyValue in _objects)
            {
                UpdateFade(keyValue.Key, keyValue.Value);
            }
            foreach (var item in _toDelete)
            {
                _objects.Remove(item);
            }
            _toDelete.Clear();
            foreach (var action in _actions)
            {
                action();
            }
            _actions.Clear();
        }

        private void UpdateFade(IShowHide fadeOut, ShowHideStruct fadeStruct)
        {
            fadeStruct.Time += Time.deltaTime;
            if (fadeStruct.Time >= TimeToChange)
            {
                if (fadeStruct.Show)
                {
                    fadeOut.SetAlpha(1);
                }
                else
                {
                    fadeOut.SetAlpha(0);
                }
                _toDelete.Add(fadeOut);
                if (fadeStruct.OnCompleted != null)
                    _actions.Add(fadeStruct.OnCompleted);
            }
            else
            {
                float newAlpha;
                if (fadeStruct.Show)
                {
                    newAlpha = fadeStruct.Time / TimeToChange;
                }
                else newAlpha = (TimeToChange - fadeStruct.Time) / TimeToChange;
                fadeOut.SetAlpha(newAlpha);
            }
        }
    }
}
