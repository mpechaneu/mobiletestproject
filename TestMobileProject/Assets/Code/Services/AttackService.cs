﻿using System;
using System.Collections.Generic;
using Code.Units;

namespace Code.Services
{
    public class AttackService
    {
        private readonly GameManager _manager;
        private UnitWrapper _attacker;
        private AnimationManager _attackAnimationManager;
        private List<UnitWrapper> _targets;
        private int _targetCounter;
        private Action _onCompleted;
        private bool _attackCompleted;
        private Dictionary<UnitWrapper, Action> _hitDelegates;
        private Dictionary<UnitWrapper, Action> _deathDelegates;

        public AttackService(GameManager manager)
        {
            _manager = manager;
        }

        public void AttackUnit(UnitWrapper attacker, List<UnitWrapper> targets, Action onCompleted)
        {
            _attacker = attacker;
            _attackAnimationManager = _attacker.Unit.GetAnimationManager();
            _targets = targets;
            _targetCounter = targets.Count;
            _attackCompleted = false;
            _onCompleted = onCompleted;
            _hitDelegates = new Dictionary<UnitWrapper, Action>();
            _deathDelegates = new Dictionary<UnitWrapper, Action>();
            _attackAnimationManager.AttackCompleted += AttackCompleted;
            _attackAnimationManager.AttackHittingCompleted += AttackHittingCompleted;
            _attackAnimationManager.AnimationState = AnimationState.Attacking;
        }

        private void AttackHittingCompleted()
        {
            _attackAnimationManager.AttackHittingCompleted -= AttackHittingCompleted;
            foreach (var target in _targets)
            {
                _manager.DealDamageService.DealDamage(_attacker, target);
                var animationManager = target.Unit.GetAnimationManager();
                var hitCompletedTarget = target;
                Action hitDelegate = delegate() { OnHitCompleted(hitCompletedTarget); };
                _hitDelegates.Add(target, hitDelegate);
                animationManager.HitCompleted += hitDelegate;
                animationManager.AnimationState = AnimationState.TakeDamage;
                if (target.Unit.Dead)
                {
                    animationManager.UnitIsDead();
                    Action deathDelegate = delegate () { OnDeathCompleted(hitCompletedTarget); };
                    _deathDelegates.Add(target, deathDelegate);
                    animationManager.DeathCompleted += deathDelegate;
                }
            }
        }

        private void AttackCompleted()
        {
            _attackAnimationManager.AttackCompleted -= AttackCompleted;
            _attackAnimationManager.AnimationState = AnimationState.Idle;
            _attackCompleted = true;
            CheckAttackFinish();
        }

        private void OnDeathCompleted(UnitWrapper unitWrapper)
        {
            unitWrapper.Unit.GetAnimationManager().DeathCompleted -= _deathDelegates[unitWrapper];
            unitWrapper.Unit.GetAnimationManager().DisableAnimations();
            _manager.ShowHideService.Hide(unitWrapper, () => OnHideCompleted(unitWrapper));
        }

        private void OnHideCompleted(UnitWrapper unitWrapper)
        {
            _targetCounter--;
            _manager.DeathService.UnitDead(unitWrapper);
            CheckAttackFinish();
        }

        private void OnHitCompleted(UnitWrapper unitWrapper)
        {
            unitWrapper.Unit.GetAnimationManager().HitCompleted -= _hitDelegates[unitWrapper];
            unitWrapper.Unit.GetAnimationManager().AnimationState = AnimationState.Idle;
            if (!unitWrapper.Unit.Dead)
            {
                _targetCounter--;
                CheckAttackFinish();
            }
        }

        private void CheckAttackFinish()
        {
            if (_attackCompleted && _targetCounter == 0)
            {
                _attackCompleted = false;
                _targetCounter = 0;
                var action = _onCompleted;
                _onCompleted = null;
                _targets = null;
                _attackAnimationManager = null;
                _attacker = null;
                _hitDelegates = null;
                _deathDelegates = null;
                _attackCompleted = false;
                _targetCounter = 0;
                if (action != null)
                {
                    action();
                }
            }
        }
    }
}
