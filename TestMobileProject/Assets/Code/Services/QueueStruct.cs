﻿using System.Collections.Generic;
using Code.Landscape;

namespace Code.Services
{
    public class QueueStruct
    {
        public List<GameCell> Path;
        public GameCell Cell;
        public int Cost;
    }
}
