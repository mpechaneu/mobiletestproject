﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Data
{
    public class UnitStructList : ScriptableObject
    {
        public List<UnitStruct> UnitList;
    }
}
