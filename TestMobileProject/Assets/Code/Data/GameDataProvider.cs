﻿using System.Collections.Generic;

namespace Code.Data
{
    public class GameDataProvider
    {
        private static GameDataProvider _provider;

        private Dictionary<string, UnitStruct> _units;

        public static GameDataProvider Instance
        {
            get
            {
                if (_provider == null)
                {
                    _provider = new GameDataProvider();
                }
                return _provider;
            }
        }

        public void Initialize(Dictionary<string, UnitStruct> units)
        {
            _units = units;
        }

        public UnitStruct GetUnit(string tag)
        {
            return _units[tag];
        }
    }
}
