﻿using System;
using UnityEngine;

namespace Code.Data
{
    [Serializable]
    public class UnitStruct
    {
        public string UnitTag;
        public int Attack;
        public int Speed;
        public int Health;
        public GameObject GameView;
        public float TurnDx;
        public string Description;
        public bool SimpleMove;
        public bool RangedAttack;
        public bool DamageResistance;
        public bool SplashAttack;
    }
}
