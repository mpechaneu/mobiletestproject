﻿using System.Collections.Generic;
using UnityEngine;

namespace Code.Data
{
    public class DataInitializer : MonoBehaviour
    {
        public UnitStructList Units;

        void Awake()
        {
            var unitDict = new Dictionary<string, UnitStruct>();
            foreach (var unitStruct in Units.UnitList)
            {
                unitDict.Add(unitStruct.UnitTag, unitStruct);
            }
            GameDataProvider.Instance.Initialize(unitDict);
        }
    }
}
