﻿using Code.Data;
using Code.Landscape;
using UnityEngine;

namespace Code.Units
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(AnimationManager))]
    public class GameUnit : MonoBehaviour
    {
        public string UnitTag { get; set; }
        private bool _ready;

        public int Hp { get; set; }
        public int Speed { get; set; }
        public int Attack { get; set; }
        public int Team { get; set; }
        public bool Dead { get; set; }
        public bool Ranged { get; set; }
        private Vector2 _baseCoords;
        private float _turnDx;
        private bool _turned;

        private SpriteRenderer _spriteRenderer;
        private AnimationManager _animationManager;
        private UnitWrapper _unitWrapper;

        public SpriteRenderer Renderer
        {
            get
            {
                if (_spriteRenderer == null)
                {
                    _spriteRenderer = GetComponent<SpriteRenderer>();
                }
                return _spriteRenderer;
            }
        }



        public void ParamInitialize(string unitTag, int hp, int speed, int attack, int team, float turnDx, bool ranged)
        {
            UnitTag = unitTag;
            Hp = hp;
            Speed = speed;
            Attack = attack;
            Team = team;
            Ranged = ranged;
            Dead = false;
            _turnDx = turnDx;
            _baseCoords = new Vector2(transform.localPosition.x, transform.localPosition.y);
            _turned = false;
            _ready = true;
            InitializeTeamTurn();
            _animationManager = GetComponent<AnimationManager>();
            _animationManager.AnimationState = AnimationState.Idle;
        }

        public void InitializeWrapper(UnitWrapper wrapper)
        {
            _unitWrapper = wrapper;
            _unitWrapper.UpdateReady(_ready);
        }

        public void InitializeTeamTurn()
        {
            if (Team == Constants.Team1)
            {
                SetTurn(false);
            }
            else
            {
                SetTurn(true);
            }
        }

        public bool IsTurned()
        {
            return _turned;
        }

        public void SetTurn(bool turned)
        {
            if (turned == _turned)
                return;
            _turned = turned;
            if (_turned)
            {
                Renderer.flipX = true;
                transform.localPosition = new Vector2(-_baseCoords.x, _baseCoords.y);
            }
            else
            {
                Renderer.flipX = false;
                transform.localPosition = _baseCoords;
            }
        }

        public AnimationManager GetAnimationManager()
        {
            return _animationManager;
        }

        public float GetHpPercent()
        {
            var percent = (float)(Hp)/GameDataProvider.Instance.GetUnit(UnitTag).Health;
            return Mathf.Clamp(percent, 0, 1);
        }

        public bool IsReady()
        {
            return _ready;
        }

        public void SetReady(bool value)
        {
            _ready = value;
            _unitWrapper.UpdateReady(_ready);
        }

        public SpriteRenderer GetSpriteRenderer()
        {
            return _spriteRenderer;
        }
    }
}
