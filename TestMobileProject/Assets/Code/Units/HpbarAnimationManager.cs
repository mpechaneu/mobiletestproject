﻿using UnityEngine;

namespace Code.Units
{
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class HpbarAnimationManager: MonoBehaviour
    {
        private const string AnimationTag = "HpbarAnimation";
        private const float OnePercent = 0.01f;

        private Animator _animator;

        public SpriteRenderer Renderer
        {
            get { return GetComponent<SpriteRenderer>(); }
        }

        private Animator Animator
        {
            get
            {
                if (_animator == null)
                {
                    _animator = GetComponent<Animator>();
                }
                return _animator;
            }
        }

        public void SetHp(float persent)
        {
            var viewPercent = Mathf.Max(0, persent - OnePercent);
            var stateInfo = Animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.IsTag("HpbarAnimation"))
            {
                Animator.Play(stateInfo.fullPathHash, 0, viewPercent);
            }
        }

        public AnimationClip GetAnimationClip(string name)
        {
            if (!Animator) return null; // no animator

            foreach (AnimationClip clip in Animator.runtimeAnimatorController.animationClips)
            {
                if (clip.name == name)
                {
                    return clip;
                }
            }
            return null; // no clip by that name
        }
    }
}
