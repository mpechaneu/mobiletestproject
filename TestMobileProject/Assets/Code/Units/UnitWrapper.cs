﻿using Code.Landscape;
using Code.Services;
using UnityEngine;

namespace Code.Units
{
    public class UnitWrapper : MonoBehaviour, IShowHide
    {
        public HpbarAnimationManager HpBar;
        public SpriteRenderer ReadySign;

        private GameUnit _unit;
        private GameMap _map;
        private GameCell _cell;

        public void InitializeUnit(GameUnit unit)
        {
            _unit = unit;
            _unit.InitializeWrapper(this);
            UpdateHpbar();
        }

        public void CellInitialize(GameMap map, GameCell cell)
        {
            _cell = cell;
            _map = map;
        }

        public void UpdateSortingOrder()
        {
            var orderValue = _map.GetSortingOrder(_cell.Dx, _cell.Dy);
            _unit.Renderer.sortingOrder = orderValue;
            HpBar.Renderer.sortingOrder = orderValue - 1;
        }

        public GameUnit Unit
        {
            get { return _unit; }
        }

        public GameCell GetCell()
        {
            return _cell;
        }

        public void SetCell(GameCell value)
        {
            _cell = value;
        }

        public void UpdateReady(bool newValue)
        {
            ReadySign.gameObject.SetActive(newValue);
        }

        public void UpdateHpbar()
        {
            HpBar.SetHp(_unit.GetHpPercent());
        }

        public void SetAlpha(float alpha)
        {
            HpBar.Renderer.color = new Color(HpBar.Renderer.color.r, HpBar.Renderer.color.g, HpBar.Renderer.color.b, alpha);
            var unitColor = Unit.GetSpriteRenderer().color;
            Unit.GetSpriteRenderer().color = new Color(unitColor.r, unitColor.g, unitColor.b, alpha);
        }
    }
}
