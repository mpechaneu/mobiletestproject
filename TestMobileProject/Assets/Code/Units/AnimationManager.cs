﻿using System;
using Code.Services;
using UnityEngine;

namespace Code.Units
{
    [RequireComponent(typeof(Animator))]
    public class AnimationManager : MonoBehaviour
    {
        private const int AltStandValue = 6;
        private const string MoveEndName = "MoveEnd";
        // parameters
        private const string Idle = "Idle";
        private const string Moving = "Moving";
        private const string Turning = "Turning";
        private const string Attacking = "Attacking";
        private const string Dead = "Dead";
        private const string TakeDamage = "Hit";
        private const string AltStand = "AltStand";
        private const string MoveEnd = "MoveEnd";

        // event parameters

        private const string AttackEventParam = "Attack";
        private const string DeathEventParam = "Death";
        private const string HitEventParam = "Hit";
        private const string MoveEventParam = "Move";
        private const string StandEventParam = "Stand";
        private const string TurnEventParam = "Turn";
        private const string MoveEndEventParam = "MoveEnd";

        public event Action AttackCompleted;
        public event Action AttackHittingCompleted;
        public event Action DeathCompleted;
        public event Action HitCompleted;
        public event Action MoveCompleted;
        public event Action TurnCompleted;
        public event Action MoveEndCompleted;

        private Animator _animator;
        private AnimationState _animationState;

        private Animator Animator
        {
            get
            {
                if (_animator == null)
                {
                    _animator = GetComponent<Animator>();
                }
                return _animator;
            }
        }

        public AnimationState AnimationState
        {
            get
            {
                return _animationState;
            }
            set 
            {
                if (_animationState == value)
                    return;
                CrearState();
                SetNewState(value);
            }
        }

        public void UnitIsDead()
        {
            Animator.SetBool(Dead, true);
        }

        public void AnimationFinished(string animationType)
        {
            switch (animationType)
            {
                case AttackEventParam:
                    if (AttackCompleted != null)
                    {
                        AttackCompleted();
                    }
                    break;
                case DeathEventParam:
                    if (DeathCompleted != null)
                    {
                        DeathCompleted();
                    }
                    break;
                case HitEventParam:
                    if (HitCompleted != null)
                    {
                        HitCompleted();
                    }
                    break;
                case MoveEventParam:
                    if (MoveCompleted != null)
                    {
                        MoveCompleted();
                    }
                    break;
                case StandEventParam:
                    if (RandomHelper.GetRandomPercent() < AltStandValue)
                    {
                        Animator.SetTrigger(AltStand);
                    }
                    break;
                case TurnEventParam:
                    if (TurnCompleted != null)
                    {
                        TurnCompleted();
                    }
                    break;
                case MoveEndEventParam:
                    if (MoveEndCompleted != null)
                    {
                        MoveEndCompleted();
                    }
                    break;
                default: throw new Exception();
            }
        }

        public void OnAttackHitting()
        {
            if (AttackHittingCompleted != null)
            {
                AttackHittingCompleted();
            }
        }

        private void CrearState()
        {
            switch (_animationState)
            {
                case AnimationState.Idle:
                    Animator.SetBool(Idle, false);
                    break;
                case AnimationState.Moving:
                    Animator.SetBool(Moving, false);
                    break;
                case AnimationState.Turning:
                    Animator.SetBool(Turning, false);
                    break;
                case AnimationState.Attacking:
                    Animator.SetBool(Attacking, false);
                    break;
                case AnimationState.Dying:
                    Animator.SetBool(Dead, false);
                    break;
                case AnimationState.TakeDamage:
                    break;
                case AnimationState.EndMoving:
                    Animator.SetBool(MoveEnd, false);
                    break;
                default: throw new Exception();
            }
        }

        private void SetNewState(AnimationState newAnimationState)
        {
            switch (newAnimationState)
            {
                case AnimationState.Idle:
                    Animator.SetBool(Idle, true);
                    break;
                case AnimationState.Moving:
                    Animator.SetBool(Moving, true);
                    break;
                case AnimationState.Turning:
                    Animator.SetBool(Turning, true);
                    break;
                case AnimationState.Attacking:
                    Animator.SetBool(Attacking, true);
                    break;
                case AnimationState.Dying:
                    Animator.SetBool(Dead, true);
                    break;
                case AnimationState.TakeDamage:
                    Animator.SetTrigger(TakeDamage);
                    break;
                case AnimationState.EndMoving:
                    Animator.SetBool(MoveEnd, true);
                    break;
                default: throw new Exception();
            }
            _animationState = newAnimationState;
        }

        public void DisableAnimations()
        {
            Animator.enabled = false;
        }

        public void EnableAnimations()
        {
            Animator.enabled = true;
        }

        public float GetAnimationTime()
        {
            return Animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        }

        public bool IsEndMovePlaying()
        {
            return Animator.GetCurrentAnimatorStateInfo(0).IsName(MoveEndName);
        }
    }
}
