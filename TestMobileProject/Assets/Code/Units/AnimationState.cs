﻿namespace Code.Units
{
    public enum AnimationState
    {
        Idle,
        Moving,
        Turning,
        Attacking,
        TakeDamage,
        Dying,
        EndMoving
    }
}
