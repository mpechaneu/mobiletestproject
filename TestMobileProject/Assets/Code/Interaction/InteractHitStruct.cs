﻿using UnityEngine;

namespace Code.Interaction
{
    public class InteractHitStruct
    {
        public GameObject HitObject;
        public Collider2D Collider;
        public SpriteRenderer Renderer;
        public IInteractable InteractComponent;
    }
}
