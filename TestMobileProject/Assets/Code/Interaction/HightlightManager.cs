﻿using System.Collections.Generic;
using System.Linq;
using Code.Landscape;
using Code.Services;
using Code.Units;

namespace Code.Interaction
{
    public class HightlightManager
    {
        private readonly GameManager _manager;
        private List<IInteractable> _hightlightedObjects;
        private HightlightState _hightlightState;

        public HightlightManager(GameManager manager)
        {
            _manager = manager;
        }

        public HightlightState GetHightlightState()
        {
            return _hightlightState;
        }

        public bool CanObjectInteract(IInteractable obj)
        {
            return _hightlightedObjects.Contains(obj);
        }

        public void Processing()
        {
            CancelHightlight();
            _hightlightedObjects = new List<IInteractable>();
            _hightlightState = HightlightState.Processing;
        }

        public void HightlightDefaultTargets()
        {
            PreHightlightStandard();
            var units = _manager.GameInfo.GetUnits(Constants.Team1).Concat(_manager.GameInfo.GetUnits(Constants.Team2)).ToList();
            AddCells(units.ConvertAll(unitWrapper => unitWrapper.GetCell()));
        }

        public void HightlightUnitTargets(UnitWrapper unit)
        {
            PreHightlightStandard();
            var rangedUnit = unit.Unit.Ranged;
            var cellsToMove = _manager.BfsService.GetCellsToMove(unit, rangedUnit);
            AddCells(cellsToMove);
            if (rangedUnit)
            {
                var enemyTargets = _manager.GameInfo.GetUnits(GameInfo.GetOppositeTeam(unit.Unit.Team));
                var cellList = enemyTargets.ToList().ConvertAll(enemy => enemy.GetCell());
                AddCells(cellList);
            }
            HightlightType1();
            var readyUnits = _manager.GameInfo.GetReadyUnits();
            AddCells(readyUnits.ConvertAll( unitWrapper => unitWrapper.GetCell()));
            unit.GetCell().GetInteractManager().HightlightType2();
        }

        private void PreHightlightStandard()
        {
            CancelHightlight();
            _hightlightedObjects = new List<IInteractable>();
            _hightlightState = HightlightState.Standard;
        }

        private void AddCells(List<GameCell> cells)
        {
            foreach (var gameCell in cells)
            {
                var interactManager = gameCell.GetInteractManager();
                _hightlightedObjects.Add(interactManager);
            }
        }

        private void HightlightType1()
        {
            foreach (var obj in _hightlightedObjects)
            {
                obj.HightlightType1();
            }
        }

        private void CancelHightlight()
        {
            if (_hightlightedObjects == null)
                return;
            foreach (var obj in _hightlightedObjects)
            {
                obj.CancelHightlight();
            }
            _hightlightedObjects = null;
        }
    }
}
