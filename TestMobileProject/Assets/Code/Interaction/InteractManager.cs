﻿using System.Collections.Generic;
using Code.Data;
using Code.Landscape;
using Code.Services;
using Code.Units;

namespace Code.Interaction
{
    public class InteractManager
    {
        private readonly GameManager _manager;
        private UnitWrapper _unitToActivate;
        private UnitWrapper _target;

        public InteractManager(GameManager manager)
        {
            _manager = manager;
        }

        public void CellToMove(GameCell gameCell)
        {
            UnitActivated();
            _manager.MoveService.MoveUnit(_unitToActivate, gameCell, ActionCompleted);
        }

        public void ActivateUnit(UnitWrapper unitWrapper)
        {
            var alreadyActivated = _unitToActivate == unitWrapper;
            CancelTargeting();
            if (!alreadyActivated)
            {
                _unitToActivate = unitWrapper;
                _manager.HightlightManager.HightlightUnitTargets(unitWrapper);
            }
        }

        public void AttackUnit(UnitWrapper target, GameCell cellAttackPosition)
        {
            _target = target;
            UnitActivated();
            if (_unitToActivate.GetCell() == cellAttackPosition)
            {
                MoveToAttackCompleted();
            }
            else
            {
                _manager.MoveService.MoveUnit(_unitToActivate, cellAttackPosition, MoveToAttackCompleted);
            }
        }

        public void RangedAttackUnit(UnitWrapper target)
        {
            _target = target;
            UnitActivated();
            MoveToAttackCompleted();
        }

        public void CancelTargeting()
        {
            _manager.HightlightManager.HightlightDefaultTargets();
            _unitToActivate = null;
            _target = null;
        }

        private void MoveToAttackCompleted()
        {
            if (MoveService.ShouldTurnToCell(_unitToActivate.GetCell(), _unitToActivate.Unit.IsTurned(), _target.GetCell()))
            {
                _manager.TurnService.TurnUnit(_unitToActivate, ProcessAttack);
            }
            else
            {
                ProcessAttack();
            }
        }

        private void ProcessAttack()
        {
            var unitInfo = GameDataProvider.Instance.GetUnit(_unitToActivate.Unit.UnitTag);
            List<UnitWrapper> targets;
            if (unitInfo.SplashAttack)
            {
                var cells = _unitToActivate.GetCell().GetNearUnitsByTeam(GameInfo.GetOppositeTeam(_unitToActivate.Unit.Team));
                targets = cells.ConvertAll(cell => cell.GetUnitOnCell());
            }
            else
            {
                targets = new List<UnitWrapper>();
                targets.Add(_target);
            }
            _manager.AttackService.AttackUnit(_unitToActivate, targets, ActionCompleted);
        }

        private void UnitActivated()
        {
            _unitToActivate.Unit.SetReady(false);
            _manager.HightlightManager.Processing();
        }

        private void ActionCompleted()
        {
            _manager.TurnService.ReturnToTeamTurn(_unitToActivate, OnTurnCompleted);
        }

        private void OnTurnCompleted()
        {
            _unitToActivate.Unit.GetAnimationManager().AnimationState = AnimationState.Idle;
            CancelTargeting();
        }

        public UnitWrapper GetUnitToActivate()
        {
            return _unitToActivate;
        }
    }
}
