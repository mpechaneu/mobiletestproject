﻿using System.Collections.Generic;

namespace Code.Interaction
{
    public class InteractComparer: Comparer<InteractHitStruct>
    {
        private readonly Dictionary<string, int> _sortingLayers;

        public InteractComparer(Dictionary<string, int> sortingLayers)
        {
            _sortingLayers = sortingLayers;
        }

        public override int Compare(InteractHitStruct x, InteractHitStruct y)
        {
            var xLayer = _sortingLayers[x.Renderer.sortingLayerName];
            var yLayer = _sortingLayers[y.Renderer.sortingLayerName];
            if (xLayer > yLayer)
                return -1;
            if (xLayer < yLayer)
                return 1;
            return 0;
        }
    }
}
