﻿using System.Collections.Generic;
using Code.Landscape;
using UnityEngine;
using AnimationState = Code.Units.AnimationState;

namespace Code.Interaction
{
    public class InputManager : MonoBehaviour
    {
        public const int SkipFrames = 3;
        public const float SlightMoveDistance = 4;

        public Camera Camera;
        public GameManager Manager;

        private Dictionary<string, int> _sortingLayers;
        private InteractHitStruct _current;
        private bool _disabled;

        private void Awake()
        {
            _sortingLayers = new Dictionary<string, int>();
            foreach (var layer in SortingLayer.layers)
            {
                _sortingLayers.Add(layer.name, layer.value);
            }
            _current = null;
            _disabled = false;
        }

        public void Disable()
        {
            _disabled = true;
        }

        void Update ()
        {
            if (_disabled)
                return;
            for (int i = 0; i < Input.touchCount; ++i)
            {
                var touch = Input.GetTouch(i);
                switch (touch.phase)
                {
                    case TouchPhase.Began:
                        ProcessTouchBegining(touch);
                        break;
                    case TouchPhase.Ended:
                        ProcessTouchEnd(touch);
                        break;
                    case TouchPhase.Moved:
                        ProcessTouchMove(touch);
                        break;
                    case TouchPhase.Canceled:
                        break;
                    case TouchPhase.Stationary:
                        ProcessTouchStationary(touch);
                        break;
                }
            }
        }

        private void ProcessTouchBegining(Touch touch)
        {
            var hitStruct = GetHitStruct(touch);
            if (hitStruct != null)
            {
                ProcessTouchOut(hitStruct);
                _current = hitStruct;
                hitStruct.InteractComponent.BeginTouch();
            }
        }

        private void ProcessTouchStationary(Touch touch)
        {
            var hitStruct = GetHitStruct(touch);
            if (hitStruct != null)
            {
                hitStruct.InteractComponent.TouchStationary();
            }
        }

        private void ProcessTouchEnd(Touch touch)
        {
            var hitStruct = GetHitStruct(touch);
            if (hitStruct != null)
            {
                ProcessTouchOut(hitStruct);
                hitStruct.InteractComponent.TouchEnded();
            }
        }

        private void ProcessTouchMove(Touch touch)
        {
            //Manager.TooltipSerivce.OnMoveMove(touch.deltaPosition);
            var hitStruct = GetHitStruct(touch);
            if (hitStruct != null)
            {
                if (_current == null || _current.InteractComponent != hitStruct.InteractComponent)
                {
                    ProcessTouchOut(hitStruct);
                    _current = hitStruct;
                    Vector2 point = Camera.ScreenToWorldPoint(touch.position);
                    hitStruct.InteractComponent.TouchMovedOver(point);
                }
            }
            else
            {
                ProcessTouchOut(null);
            }
        }

        private InteractHitStruct GetHitStruct(Touch touch)
        {
            var hit = GetHitArray(touch);
            if (hit.Length == 0)
            {
                return null;
            }
            return GetHitStruct(hit);
        }

        private void ProcessTouchOut(InteractHitStruct hitStruct)
        {
            if (_current != null)
            {
                if (hitStruct == null || _current.InteractComponent != hitStruct.InteractComponent)
                {
                    _current.InteractComponent.TouchMovedOut();
                    _current = null;
                }
            }
        }

        private RaycastHit2D[] GetHitArray(Touch touch)
        {
            Vector3 worldPoint = Camera.ScreenToWorldPoint(touch.position);
            worldPoint.z = Camera.transform.position.z;
            Ray ray = new Ray(worldPoint, new Vector3(0, 0, 1));
            return Physics2D.GetRayIntersectionAll(ray, Mathf.Abs(worldPoint.z) + 1);
        }

        private InteractHitStruct GetHitStruct(RaycastHit2D[] hit)
        {
            int i;
            List<InteractHitStruct> hitList = new List<InteractHitStruct>();
            for (i = 0; i < hit.Length; i++)
            {
                var hitStruct = new InteractHitStruct()
                {
                    InteractComponent = hit[i].collider.gameObject.GetComponent<IInteractable>(),
                    Renderer = hit[i].collider.gameObject.GetComponent<SpriteRenderer>(),
                    Collider = hit[i].collider,
                    HitObject = hit[i].collider.gameObject
                };
                if (Manager.HightlightManager.CanObjectInteract(hitStruct.InteractComponent))
                {
                    hitList.Add(hitStruct);
                }
            }
            if (hitList.Count == 0)
                return null;
            InteractComparer comparer = new InteractComparer(_sortingLayers);
            hitList.Sort(comparer);
            return hitList[0];
        }
    }
}
