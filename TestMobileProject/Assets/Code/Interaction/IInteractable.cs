﻿using UnityEngine;

namespace Code.Interaction
{
    public interface IInteractable
    {
        void BeginTouch();
        void TouchMovedOver(Vector2 position);
        void TouchMovedOut();
        void TouchEnded();
        void TouchStationary();
        void HightlightType1();
        void HightlightType2();
        void CancelHightlight();
    }
}
