﻿namespace Code.Interaction
{
    public enum HightlightState
    {
        Standard,
        SpellTarget,
        Processing
    }
}
