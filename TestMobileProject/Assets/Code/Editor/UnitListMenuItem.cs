﻿using Code.Data;
using UnityEditor;
using UnityEngine;

namespace Code.Editor
{
    public class UnitListMenuItem : MonoBehaviour
    {

        [MenuItem("Assets/Create/Custom/UnitListMenuItem")]
        public static void CreateAsset()
        {
            CustomAssetUtility.CreateAsset<UnitStructList>();
        }
    }
}
