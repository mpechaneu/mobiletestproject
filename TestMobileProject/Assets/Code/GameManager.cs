﻿using System;
using System.Linq;
using Code.Interaction;
using Code.Landscape;
using Code.Services;
using UnityEngine;
using UnityEngine.UI;

namespace Code
{
    [RequireComponent(typeof(UnitCreateManager))]
    public class GameManager : MonoBehaviour
    {
        public const int Width = 1280;
        public const int Height = 768;

        public Text FpsText;
        public GameMap Map;
        public BfsService BfsService;
        public HightlightManager HightlightManager;
        public InputManager InputManager;
        public TurnService TurnService;
        public GameInfo GameInfo;
        public InteractManager InteractManager;
        public RoundService RoundService;
        public MoveService MoveService;
        public CellMoveService CellMoveService;
        public AttackService AttackService;
        public DealDamageService DealDamageService;
        public ShowHideService ShowHideService;
        public DeathService DeathService;
        public ArrowPositionService ArrowPositionService;
        public TooltipSerivce TooltipSerivce;
        public GameEndService GameEndService;

        // Use this for initialization
        void Start () {
            Screen.SetResolution(Width, Height, false);
            GameInfo = new GameInfo();
            BfsService = new BfsService(this);
            HightlightManager = new HightlightManager(this);
            TurnService = new TurnService(this);
            InteractManager = new InteractManager(this);
            MoveService = new MoveService(this);
            AttackService = new AttackService(this);
            DeathService.Initialize(this);
            DealDamageService = new DealDamageService(this);
            ShowHideService.Initialize(this);
            Map.Initialize(this);
            RoundService.StartRound(Constants.Team2);
        }
	
        // Update is called once per frame
        void Update () {
            FpsText.text = String.Format("FPS: {0}", (float)1 / Time.deltaTime);
        }

        public UnitCreateManager GetUnitCreateManager()
        {
            return GetComponent<UnitCreateManager>();
        }
    }
}
