﻿using System.Linq;
using Boo.Lang;
using Code.Units;
using UnityEngine;
using AnimationState = Code.Units.AnimationState;

namespace Code.Landscape
{
    public class GameMap : MonoBehaviour
    {
        private const int SortingOrderModifier = 10;

        public GameCell[] Row1Cells;
        public GameCell[] Row2Cells;
        public GameCell[] Row3Cells;
        public GameCell[] Row4Cells;
        public GameCell[] Row5Cells;

        private List<List<GameCell>> _mapCells;
        private int _mapWidth;
        private int _mapHeight;
        private GameManager _manager;

        public void Initialize(GameManager manager)
        {
            _manager = manager;
            _mapCells = new List<List<GameCell>>();
            var rows = new List<GameCell[]> { Row1Cells, Row2Cells, Row3Cells, Row4Cells, Row5Cells};
            int i, k;
            _mapHeight = rows.Count;
            _mapWidth = Row2Cells.Length;
            bool bigLine = false;
            for (i = 0; i < rows.Count; i++)
            {
                var mapRow = new List<GameCell>();
                _mapCells.Add(mapRow);
                for (k = 0; k < rows[i].Length; k++)
                {
                    var cell = rows[i][k];
                    mapRow.Add(cell);
                    cell.InitializeCell(k, i, this, bigLine);
                }
                bigLine = !bigLine;
            }
            AddUnit("unicorn", 1, 4, Constants.Team2);
            AddUnit("knight", 1, 2, Constants.Team2);
            AddUnit("peasant", 1, 0, Constants.Team2);
            AddUnit("necromancer", 3, 0, Constants.Team1);
            AddUnit("hydra", 3, 2, Constants.Team1);
            AddUnit("minotaur", 3, 4, Constants.Team1);
        }

        public GameCell GetCell(int dx, int dy)
        {
            return _mapCells[dy][dx];
        }

        public int GetMapWidth()
        {
            return _mapWidth;
        }

        public int GetMapHeight()
        {
            return _mapHeight;
        }

        public UnitWrapper AddUnit(string unitTag, int dx, int dy, int team)
        {
            return GetCell(dx, dy).InitializeCellWithUnit(unitTag, team);

        }

        public int GetSortingOrder(int dx, int dy)
        {
            return SortingOrderModifier *( dx + (GetMapHeight() - dy) * SortingOrderModifier);
        }

        public UnitCreateManager CreateManager()
        {
            return _manager.GetUnitCreateManager();;
        }

        public GameManager GetGameManager()
        {
            return _manager;
        }

        public List<GameCell> GetCells()
        {
            var resultArray = new List<GameCell>();
            foreach (var cellList in _mapCells)
            {
                foreach (var gameCell in cellList)
                {
                    resultArray.Add(gameCell);
                }
            }
            return resultArray;
        } 
    }
}
