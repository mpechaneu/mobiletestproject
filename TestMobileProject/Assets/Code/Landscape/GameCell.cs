﻿using System;
using System.Collections.Generic;
using System.Linq;
using Code.Data;
using Code.Units;
using UnityEngine;

namespace Code.Landscape
{
    [RequireComponent(typeof(CellInteractManager))]
    public class GameCell : MonoBehaviour
    {
        private const int SmallLineModifier = 1;
        private const int CellEdges = 6;
        public const int TopLeft = 0;
        public const int TopRight = 1;
        public const int Right = 2;
        public const int BottomRight = 3;
        public const int BottomLeft = 4;
        public const int Left = 5;

        public CellType CellType;
        public GameObject Obstacle;

        private GameMap _map;
        private int _dx;
        private int _dy;
        private bool[] _hasCellAround;
        private bool _bigLine;
        private UnitWrapper _unitOnCell;
        private CellInteractManager _interactManager;

        public int Dx
        {
            get { return _dx; }
        }

        public int Dy
        {
            get { return _dy; }
        }

        public void InitializeCell(int dx, int dy, GameMap map, bool bigLine)
        {
            _map = map;
            _dx = dx;
            _dy = dy;
            _bigLine = bigLine;
            _interactManager = GetComponent<CellInteractManager>();
            InitializeAvailableCells();
            InitializeCellType();
            GetInteractManager().Initialize(map.GetGameManager());
        }

        private void InitializeCellType()
        {
            switch (CellType)
            {
                case CellType.Cell:
                   break;
                case CellType.Obstacle:
                   Obstacle.GetComponent<SpriteRenderer>().sortingOrder = _map.GetSortingOrder(_dx, _dy);
                   break;
                case CellType.ResourceCell:
                   break;
                default: throw new Exception();
            }
        }

        private void InitializeAvailableCells()
        {
            _hasCellAround = new bool[CellEdges] { true, true, true, true, true, true};
            if (_dy == 0)
            {
                _hasCellAround[BottomRight] = false;
                _hasCellAround[BottomLeft] = false;
            }
            if (_dy == _map.GetMapHeight() - 1)
            {
                _hasCellAround[TopLeft] = false;
                _hasCellAround[TopRight] = false;
            }
            if (_dx == 0)
            {
                if (_bigLine)
                {
                    _hasCellAround[Left] = false;
                    _hasCellAround[TopLeft] = false;
                    _hasCellAround[BottomLeft] = false;
                }
                else
                {
                    _hasCellAround[Left] = false;
                }
            }
            var lineModifier = 0;
            if (!_bigLine)
                lineModifier = SmallLineModifier;
            if (_dx == _map.GetMapWidth() - 1 - lineModifier)
            {
                if (_bigLine)
                {
                    _hasCellAround[Right] = false;
                    _hasCellAround[TopRight] = false;
                    _hasCellAround[BottomRight] = false;
                }
                else
                {
                    _hasCellAround[Right] = false;
                }
            }
        }

        public UnitWrapper InitializeCellWithUnit(string unitTag, int team)
        {
            if (IsCellOccupied())
                throw new Exception("cell is occupied");
            var unitWrapper = _map.CreateManager().CreateUnit(unitTag, transform, team);
            unitWrapper.CellInitialize(_map, this);
            AddUnit(unitWrapper);
            return unitWrapper;
        }

        public List<GameCell> GetEmptyNearCells()
        {
            var nearCells = GetNearCells();
            return nearCells.Where(cell => !cell.IsCellOccupied()).ToList();
        }

        public List<GameCell> GetNearUnitsByTeam(int team)
        {
            var nearCells = GetNearCells();
            return nearCells.Where(cell => cell._unitOnCell != null &&
                                   cell._unitOnCell.Unit.Team == team).ToList();
        } 

        public List<GameCell> GetNearCells()
        {
            var resultList = new List<GameCell>();
            int i;
            for (i=0; i<_hasCellAround.Length; i++)
            {
                var cell = GetNearCell(i);
                if (cell != null)
                {
                    resultList.Add(cell);
                }
            }
            return resultList;
        }

        public GameCell GetNearCell(int direction)
        {
            int dx, dy;
            if (_hasCellAround[direction])
            {
                switch (direction)
                {
                    case TopLeft:
                        if (_bigLine)
                        {
                            dx = _dx - 1;
                            dy = _dy + 1;
                        }
                        else
                        {
                            dx = _dx;
                            dy = _dy + 1;
                        }
                        break;
                    case TopRight:
                        if (_bigLine)
                        {
                            dx = _dx;
                            dy = _dy + 1;
                        }
                        else
                        {
                            dx = _dx + 1;
                            dy = _dy + 1;
                        }
                        break;
                    case Right:
                        dy = _dy;
                        dx = _dx + 1;
                        break;
                    case BottomRight:
                        if (_bigLine)
                        {
                            dx = _dx;
                            dy = _dy - 1;
                        }
                        else
                        {
                            dx = _dx + 1;
                            dy = _dy - 1;
                        }
                        break;
                    case BottomLeft:
                        if (_bigLine)
                        {
                            dx = _dx - 1;
                            dy = _dy - 1;
                        }
                        else
                        {
                            dx = _dx;
                            dy = _dy - 1;
                        }
                        break;
                    case Left:
                        dy = _dy;
                        dx = _dx - 1;
                        break;
                    default: throw new Exception();
                }
                return _map.GetCell(dx, dy);
            }
            return null;
        }

        public List<int> GetNearDirections()
        {
            var resultList = new List<int>();
            int i;
            for (i = 0; i < _hasCellAround.Length; i++)
            {
                if (_hasCellAround[i])
                {
                    resultList.Add(i);
                }
            }
            return resultList;
        }

        private bool IsCellOccupied()
        {
            return CellType == CellType.Obstacle || _unitOnCell != null;
        }

        public CellInteractManager GetInteractManager()
        {
            return _interactManager;
        }

        public UnitWrapper GetUnitOnCell()
        {
            return _unitOnCell;
        }

        public bool IsOnBigline()
        {
            return _bigLine;
        }

        public void ClearUnit()
        {
            _unitOnCell.SetCell(null);
            _unitOnCell = null;
        }

        public void AddUnit(UnitWrapper unit)
        {
            _unitOnCell = unit;
            _unitOnCell.SetCell(this);
            _unitOnCell.UpdateSortingOrder();
        }
    }
}
