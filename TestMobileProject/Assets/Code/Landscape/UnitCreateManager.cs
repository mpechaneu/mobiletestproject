﻿using Code.Data;
using Code.Units;
using UnityEngine;

namespace Code.Landscape
{
    [RequireComponent(typeof(GameManager))]
    public class UnitCreateManager: MonoBehaviour
    {
        public GameObject UnitWrapper;

        private GameManager Manager
        {
            get
            {
                return GetComponent<GameManager>();
            }
        }

        public UnitWrapper CreateUnit(string unitTag, Transform initializeTransform, int team)
        {
            var wrapperObj = Instantiate(UnitWrapper, initializeTransform, false);
            var wrapper = wrapperObj.GetComponent<UnitWrapper>();
            var unitStruct = GameDataProvider.Instance.GetUnit(unitTag);
            var createdObj = Instantiate(unitStruct.GameView, wrapperObj.transform, false);
            var gameUnit = createdObj.GetComponent<GameUnit>();
            gameUnit.ParamInitialize(unitTag, unitStruct.Health, unitStruct.Speed, unitStruct.Attack, team, unitStruct.TurnDx, unitStruct.RangedAttack);
            wrapper.InitializeUnit(gameUnit);
            Manager.GameInfo.AddUnit(wrapper);
            return wrapper;
        }
    }
}
