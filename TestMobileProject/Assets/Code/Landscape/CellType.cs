﻿namespace Code.Landscape
{
    public enum CellType
    {
        Cell,
        ResourceCell,
        Obstacle
    }
}
