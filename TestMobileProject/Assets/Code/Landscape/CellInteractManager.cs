﻿using System;
using Code.Interaction;
using Code.Services;
using Code.Units;
using UnityEngine;

namespace Code.Landscape
{
    [RequireComponent(typeof(PolygonCollider2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class CellInteractManager : MonoBehaviour, IInteractable
    {
        private const float MouseOverColor = 0.5f;
        private const float HightlightColorType1 = 0.7f;
        private const float DisabledHightlight = 1f;

        private SpriteRenderer _renderer;
        private GameCell _cell;
        private GameManager _manager;
        private HightlightCellState _hightlightState;

        private SpriteRenderer Renderer
        {
            get
            {
                if (_renderer == null)
                {
                    _renderer = GetComponent<SpriteRenderer>();
                }
                return _renderer;
            }
        }

        private GameCell Cell
        {
            get
            {
                if (_cell == null)
                {
                    _cell = GetComponent<GameCell>();
                }
                return _cell;
            }
        }

        public void Initialize(GameManager manager)
        {
            _manager = manager;
            _hightlightState = HightlightCellState.None;
        }

        public void BeginTouch()
        {
            var hightlightState = _manager.HightlightManager.GetHightlightState();
            switch (hightlightState)
            {
                case HightlightState.Standard:
                    ProcessBeginTouchStandard();
                    break;
            }
            switch (_hightlightState)
            {
                case HightlightCellState.Hightlighted:
                    MouseOver();
                    break;
            }
        }

        private void ProcessBeginTouchStandard()
        {
            var unitOnCell = Cell.GetUnitOnCell();
            if (unitOnCell == null)
            {
                return;
            }
            if (unitOnCell.Unit.Team == _manager.GameInfo.PlayerTurn && unitOnCell.Unit.IsReady())
            {
                _manager.InteractManager.ActivateUnit(unitOnCell);
            }
        }

        public void TouchMovedOver(Vector2 coords)
        {
            switch (_hightlightState)
            {
                case HightlightCellState.None:
                    break;
                case HightlightCellState.Hightlighted:
                    MouseOver();
                    var unitToActivate = _manager.InteractManager.GetUnitToActivate();
                    var unitOnCell = GetCell().GetUnitOnCell();
                    if (!unitToActivate.Unit.Ranged && unitOnCell != null && unitOnCell.Unit.Team != _manager.GameInfo.PlayerTurn)
                    {
                        _manager.ArrowPositionService.ViewPositionArrow(coords, GetCell());
                    }
                    break;
            }
        }

        public void TouchMovedOut()
        {
            switch (_hightlightState)
            {
                case HightlightCellState.None:
                    break;
                case HightlightCellState.Hightlighted:
                    HightlightType1();
                    break;
            }
            _manager.ArrowPositionService.RemoveArrow();
            _manager.TooltipSerivce.HideTooltip();
        }

        public void TouchEnded()
        {
            _manager.TooltipSerivce.HideTooltip();
            var unitOnCell = Cell.GetUnitOnCell();
            if (unitOnCell == null)
            {
                _manager.InteractManager.CellToMove(Cell);
                return;
            }
            if (unitOnCell.Unit.Team != _manager.GameInfo.PlayerTurn)
            {
                if (_hightlightState == HightlightCellState.None)
                    return;
                var unitToActivate = _manager.InteractManager.GetUnitToActivate();
                if (unitToActivate.Unit.Ranged)
                {
                    _manager.InteractManager.RangedAttackUnit(unitOnCell);
                }
                else
                {
                    ProcessMelleeAttack(unitOnCell);
                }

            }
            _manager.ArrowPositionService.RemoveArrow();
        }

        private void ProcessMelleeAttack(UnitWrapper unitOnCell)
        {
            GameCell cellToMove;
            if (_manager.ArrowPositionService.IsArrowInitialized())
            {
                cellToMove = _manager.ArrowPositionService.GetArrowCell();
            }
            else
            {
                cellToMove = GetCellToMove(unitOnCell.GetCell());
            }
            _manager.InteractManager.AttackUnit(unitOnCell, cellToMove);
        }

        private GameCell GetCellToMove(GameCell enemyCell)
        {
            var unitToActivate = _manager.InteractManager.GetUnitToActivate();
            var unitToActovateCell = unitToActivate.GetCell();
            if (enemyCell.GetNearCells().Contains(unitToActovateCell))
                return unitToActovateCell;
            var path = _manager.BfsService.FindPath(unitToActivate, null, enemyCell.GetUnitOnCell(), unitToActivate.Unit.Speed);
            return path[path.Count - 1];
        }

        public void TouchStationary()
        {
            var unitOnCell = Cell.GetUnitOnCell();
            if (unitOnCell != null)
            {
                _manager.TooltipSerivce.StartViewingTooltip(unitOnCell.Unit);
            }
        }

        public void HightlightType1()
        {
            _hightlightState = HightlightCellState.Hightlighted;
            Renderer.color = new Color(HightlightColorType1, HightlightColorType1, HightlightColorType1);
        }

        public void HightlightType2()
        {
            Cell.GetUnitOnCell().Unit.GetAnimationManager().DisableAnimations();
        }

        private void MouseOver()
        {
            Renderer.color = new Color(MouseOverColor, MouseOverColor, 1);
        }

        private void RemoveColor()
        {
            Renderer.color = new Color(DisabledHightlight, DisabledHightlight, DisabledHightlight);
        }

        public void CancelHightlight()
        {
            RemoveColor();
            var unitOnCell = Cell.GetUnitOnCell();
            if (unitOnCell != null)
            {
                unitOnCell.Unit.GetAnimationManager().EnableAnimations();
            }
            _hightlightState = HightlightCellState.None;
        }

        public GameCell GetCell()
        {
            return GetComponent<GameCell>();
        }

        public HightlightCellState GetHightlightCellState()
        {
            return _hightlightState;
        }
    }
}
